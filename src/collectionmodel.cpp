/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "collectionmodel.h"

#include <QDebug>

#include <KConfigGroup>

CollectionModel::CollectionModel()
    : QAbstractListModel()
{
    m_config = KSharedConfig::openConfig();

    KConfigGroup general = m_config->group("General");
    m_collections = general.readEntry<QStringList>("Collections", QStringList{});
}

CollectionModel::~CollectionModel()
{
}

QVariant CollectionModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case NameRole:
        return m_collections[index.row()];
    case ItemsRole:
        const QString name = m_collections[index.row()];
        return m_config->group(name).readEntry("items", QStringList{});
    }

    return QStringLiteral("Foo");
}

int CollectionModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_collections.size();
}

QHash<int, QByteArray> CollectionModel::roleNames() const
{
    return {
        {NameRole, "name"},
        {ItemsRole, "items"},
    };
}

void CollectionModel::addItemToCollection(const QString &collectionName, const QString &item)
{
    KConfigGroup group = m_config->group(collectionName);

    QStringList items = group.readEntry("items", QStringList{});
    items << item;

    group.writeEntry("items", items);

    const int idx = m_collections.indexOf(collectionName);

    Q_EMIT dataChanged(index(idx, 0), index(idx, 0), {});
}

void CollectionModel::addCollection(const QString &name)
{
    beginInsertRows({}, m_collections.size(), m_collections.size());
    m_collections.append(name);
    KConfigGroup general = m_config->group("General");
    general.writeEntry("Collections", m_collections);
    endInsertRows();
}

void CollectionModel::removeCollection(int index)
{
    beginRemoveRows({}, index, index);

    m_collections.removeAt(index);
    KConfigGroup general = m_config->group("General");
    general.writeEntry("Collections", m_collections);

    endRemoveRows();
}
