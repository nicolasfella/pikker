/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "pikkercontroller.h"

#include <QDebug>
#include <QRandomGenerator>

#include <KConfigGroup>
#include <KSharedConfig>

PikkerController::PikkerController(QObject *parent)
    : QObject(parent)
{
    m_config = KSharedConfig::openConfig();
}

void PikkerController::addAvailableItem(const QString &collectionName, const QString &item)
{
    m_availableItems.append(item);
    Q_EMIT availableItemsChanged();
}

QString PikkerController::serializeAvailableItems() const
{
    return m_availableItems.join(QLatin1Char(';'));
}

QStringList PikkerController::unselectedItems() const
{
    QStringList result;
    for (const QString &item : qAsConst(m_availableItems)) {
        if (!m_myPositives.contains(item) && !m_myNegatives.contains(item)) {
            result << item;
        }
    }

    return result;
}

void PikkerController::pickPositive(const QString &item)
{
    m_myPositives << item;
    Q_EMIT unselectedItemsChanged();
    Q_EMIT commonItemsChanged();
}

void PikkerController::pickNegative(const QString &item)
{
    m_myNegatives << item;
    Q_EMIT unselectedItemsChanged();
    Q_EMIT commonItemsChanged();
}

QStringList PikkerController::commonItems() const
{
    QStringList result;

    for (const QString &item : qAsConst(m_myPositives)) {
        if (m_otherSelection.contains(item)) {
            result << item;
        }
    }

    return result;
}

QString PikkerController::randomCommonItem() const
{
    const QStringList items = commonItems();

    if (items.isEmpty()) {
        return QString();
    }

    const int index = QRandomGenerator::global()->bounded(items.size());

    return items[index];
}

QStringList PikkerController::otherSelection() const
{
    return m_otherSelection;
}

void PikkerController::setOtherSelection(const QStringList &otherSelection)
{
    if (m_otherSelection != otherSelection) {
        m_otherSelection = otherSelection;
        Q_EMIT otherSelectionChanged();
        Q_EMIT commonItemsChanged();
    }
}

void PikkerController::startSession(const QString &collectionName)
{
    KConfigGroup collectionGroup = m_config->group(collectionName);

    m_availableItems = collectionGroup.readEntry("items", QStringList{});
    Q_EMIT availableItemsChanged();
}
