#include "secondarysession.h"

QStringList SecondarySession::unselectedItems() const
{
    QStringList result;
    for (const QString &item : qAsConst(m_availableItems)) {
        if (!m_myPositives.contains(item) && !m_myNegatives.contains(item)) {
            result << item;
        }
    }

    return result;
}

void SecondarySession::pickPositive(const QString &item)
{
    m_myPositives << item;
    Q_EMIT unselectedItemsChanged();
    Q_EMIT positivesChanged();
}

void SecondarySession::pickNegative(const QString &item)
{
    m_myNegatives << item;
    Q_EMIT unselectedItemsChanged();
}
