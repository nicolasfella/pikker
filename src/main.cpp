/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QImage>

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#else
#include <QApplication>
#include <QQuickStyle>
#endif

#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "QrCodeScannerFilter.h"
#include "collectionmodel.h"
#include "pikkercontroller.h"
#include "secondarysession.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
#ifdef Q_OS_ANDROID
    QGuiApplication app(argc, argv);
#else
    QApplication app(argc, argv);
    // Default to org.kde.desktop style unless the user forces another style
    if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
        QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
    }
#endif

#ifdef Q_OS_WINDOWS
    QApplication::setStyle(QStringLiteral("breeze"));
#endif

    QCoreApplication::setApplicationName(QStringLiteral("pikker"));
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("kde.org"));
    QCoreApplication::setApplicationVersion(QStringLiteral("0.1"));

    QGuiApplication::setApplicationDisplayName(QStringLiteral("Pikker"));
    QGuiApplication::setDesktopFileName(QStringLiteral("org.kde.pikker"));

    KLocalizedString::setApplicationDomain("pikker");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));

    KAboutData about(QStringLiteral("pikker"),
                     i18n("Pikker"),
                     QStringLiteral("0.1"),
                     i18n("Helps you pick things"),
                     KAboutLicense::GPL,
                     i18n("© 2021 KDE Community"));
    about.addAuthor(i18n("Nicolas Fella"), QString(), QStringLiteral("nicolas.fella@kde.org"));
    //     about.setProgramLogo(QImage(QStringLiteral(":/pikker.svg")));
    KAboutData::setApplicationData(about);

    qmlRegisterType<QrCodeScannerFilter>("org.kde.pikker", 1, 0, "QrCodeScannerFilter");

    PikkerController controller;
    qmlRegisterSingletonInstance("org.kde.pikker", 1, 0, "PikkerController", &controller);

    CollectionModel collections;
    qmlRegisterSingletonInstance("org.kde.pikker", 1, 0, "CollectionModel", &collections);

    qmlRegisterType<SecondarySession>("org.kde.pikker", 1, 0, "SecondarySession");

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
