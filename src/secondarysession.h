#pragma once

#include <QObject>

class SecondarySession : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList availableItems MEMBER m_availableItems NOTIFY availableItemsChanged)
    Q_PROPERTY(QStringList unselectedItems READ unselectedItems NOTIFY unselectedItemsChanged)
    Q_PROPERTY(QStringList positives MEMBER m_myPositives NOTIFY positivesChanged)

public:
    QStringList unselectedItems() const;

    Q_INVOKABLE void pickPositive(const QString &item);
    Q_INVOKABLE void pickNegative(const QString &item);

Q_SIGNALS:
    void availableItemsChanged();
    void unselectedItemsChanged();
    void positivesChanged();

private:
    QStringList m_availableItems;
    QStringList m_myPositives;
    QStringList m_myNegatives;
};
