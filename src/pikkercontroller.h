/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#pragma once

#include <QObject>

#include <KSharedConfig>

class PikkerController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList availableItems MEMBER m_availableItems NOTIFY availableItemsChanged)
    Q_PROPERTY(QStringList myPositives MEMBER m_myPositives)
    Q_PROPERTY(QStringList myNegatives MEMBER m_myNegatives)
    Q_PROPERTY(QStringList otherSelection READ otherSelection WRITE setOtherSelection NOTIFY otherSelectionChanged)
    Q_PROPERTY(QStringList unselectedItems READ unselectedItems NOTIFY unselectedItemsChanged)
    Q_PROPERTY(QStringList commonItems READ commonItems NOTIFY commonItemsChanged STORED false)

public:
    explicit PikkerController(QObject *parent = nullptr);

    Q_INVOKABLE void addAvailableItem(const QString &collectionName, const QString &item);
    Q_INVOKABLE QString serializeAvailableItems() const;
    Q_INVOKABLE void pickPositive(const QString &item);
    Q_INVOKABLE void pickNegative(const QString &item);
    Q_INVOKABLE QString randomCommonItem() const;
    Q_INVOKABLE void startSession(const QString &collectionName);

    QStringList unselectedItems() const;
    QStringList commonItems() const;

    QStringList otherSelection() const;
    void setOtherSelection(const QStringList &otherSelection);

Q_SIGNALS:
    void availableItemsChanged();
    void unselectedItemsChanged();
    void commonItemsChanged();
    void otherSelectionChanged();

private:
    QStringList m_availableItems;
    QStringList m_myPositives;
    QStringList m_myNegatives;
    QStringList m_otherSelection;

    KSharedConfigPtr m_config;
};
