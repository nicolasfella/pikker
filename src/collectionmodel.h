/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#ifndef COLLECTIONMODEL_H
#define COLLECTIONMODEL_H

#include <QAbstractListModel>

#include <KConfig>
#include <KSharedConfig>

class CollectionModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        NameRole = Qt::DisplayRole,
        ItemsRole = Qt::UserRole + 1,
    };

    CollectionModel();

    ~CollectionModel();

    QVariant data(const QModelIndex &index, int role) const override;

    int rowCount(const QModelIndex &parent) const override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void addItemToCollection(const QString &collectionName, const QString &item);
    Q_INVOKABLE void addCollection(const QString &name);
    Q_INVOKABLE void removeCollection(int index);

private:
    KSharedConfigPtr m_config;
    QStringList m_collections;
};

#endif // COLLECTIONMODEL_H
