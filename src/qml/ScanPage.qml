/**
 * SPDX-FileCopyrightText: 2016-2019 Kaidan developers and contributors
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.0
import QtQuick.Controls 2.3 as Controls
import QtMultimedia 5.9
import org.kde.kirigami 2.2 as Kirigami
import QtQuick.Layouts 1.3

import org.kde.pikker 1.0

Kirigami.Page {

    id: root

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    title: qsTr("Scan QR code")

    property bool done: false

    signal scanned(string text)

    VideoOutput {
        id: viewfinder
        anchors.fill: parent
        source: camera
        autoOrientation: true
        filters: [scannerFilter]
    }

    QrCodeScannerFilter {
        id: scannerFilter
        onScanningSucceeded: result => {

            if (done) {
                return
            }

            done = true

            root.scanned(result)
        }
        onUnsupportedFormatReceived: {
            passiveNotification(qsTr("The camera format '%1' is not supported.").arg(format))
        }
    }

    Camera {
        id: camera
        focus {
            focusMode: Camera.FocusContinuous
            focusPointMode: Camera.FocusPointCenter
        }
    }

    Component.onCompleted: {
        scannerFilter.setCameraDefaultVideoFormat(camera);
        //scanned("Brot;Kuchen;Quark")
    }
}
