/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import org.kde.kirigami 2.15 as Kirigami

import org.kde.pikker 1.0

Kirigami.ScrollablePage {

    title: i18n("Pick a Collection")

    Component {
        id: scanPage

        ScanPage {
            onScanned: result => {
                PikkerController.otherSelection = result.split(";")
                pageStack.push(Qt.resolvedUrl("MergePage.qml"))
            }
        }
    }

    Component {
        id: swipePage

        SwipePage {
            session: PikkerController

            onFinished: {
                pageStack.push(scanPage)
            }
        }
    }

    Component {
        id: qrCodePage

        QrCodePage {
            content: PikkerController.serializeAvailableItems()

            actions.main: Kirigami.Action {
                text: i18n("Next")
                iconName: "go-next"
                onTriggered: pageStack.push(swipePage)
            }
        }
    }

    ListView {
        model: CollectionModel

        delegate: Kirigami.BasicListItem {
            text: name
            onClicked: {
                PikkerController.startSession(name)
                pageStack.push(qrCodePage)
            }
        }
    }
}
