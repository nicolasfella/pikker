/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import org.kde.kirigami 2.15 as Kirigami

import org.kde.pikker 1.0

Kirigami.ScrollablePage {
    id: page

    title: i18n("Manage Items")

    actions.contextualActions: [
        Kirigami.Action {
            text: i18n("Remove Session")
            iconName: "delete"
            onTriggered: {
                pageStack.pop()
                CollectionModel.removeCollection(collection.index)
            }
        }
    ]

    required property var collection

    ListView {
        id: lv
        model: page.collection.items
        delegate: Kirigami.SwipeListItem {

            Label {
                text: modelData
            }

            actions: [
                Kirigami.Action {
                    text: i18n("Remove")
                    iconName: "list-remove"
                }
            ]

        }

        Kirigami.PlaceholderMessage {
            anchors.centerIn: parent
            visible: lv.count === 0
            text: i18n("No items")
        }

    }

    footer: TextField {
        placeholderText: i18n("Add item...")
        onAccepted: {
            CollectionModel.addItemToCollection(page.collection.name, text)
            clear()
        }
        validator: RegularExpressionValidator { regularExpression: /.+/ } // Not empty
    }
}
