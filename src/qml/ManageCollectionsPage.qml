/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import org.kde.kirigami 2.15 as Kirigami

import org.kde.pikker 1.0

Kirigami.ScrollablePage {

    title: i18n("Manage Collections")

    ListView {
        model: CollectionModel

        delegate: Kirigami.BasicListItem {
            text: name
            onClicked: pageStack.push(Qt.resolvedUrl("EditCollectionPage.qml"), {collection: model})
        }
    }

    Kirigami.OverlaySheet {
        id: sheet
        header: Kirigami.Heading {
            text: i18n("Session name")
        }

        TextField {
            id: tf
        }

        footer: RowLayout {
            Button {
                text: i18n("Ok")
                icon.name: "dialog-ok"
                enabled: tf.text.length > 0
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    CollectionModel.addCollection(tf.text)
                    sheet.close()
                }
            }
        }

    }

    actions.main: Kirigami.Action {
        text: i18n("Add")
        icon.name: "list-add"
        onTriggered: {
            sheet.open()
            tf.clear()
        }
    }
}

