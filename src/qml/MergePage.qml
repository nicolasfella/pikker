/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.15 as Kirigami
import org.kde.prison 1.0 as Prison

import org.kde.pikker 1.0

Kirigami.Page {

    id: root

    title: i18n("Results")

    actions.main: Kirigami.Action {
        text: i18n("Finish")
        iconName: "dialog-ok"
        onTriggered: {
            pageStack.clear()
            pageStack.push(Qt.resolvedUrl("StartPage.qml"))
        }
    }

    Kirigami.PlaceholderMessage {
        anchors.centerIn: parent
        text: {
            if (PikkerController.commonItems.length === 0) {
                return i18n("No match")
            }

            return i18n("It's a match: %1", PikkerController.randomCommonItem())
        }
    }
}

