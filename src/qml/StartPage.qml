/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import org.kde.kirigami 2.15 as Kirigami

import org.kde.pikker 1.0

Kirigami.Page {

    title: i18n("Start")

    Component {
        id: secondaryQrCode

        QrCodePage {
            actions.main: Kirigami.Action {
                text: i18n("Done")
                iconName: "dialog-ok"
                onTriggered: {
                    pageStack.clear()
                    pageStack.push(Qt.resolvedUrl("StartPage.qml"))
                }
            }
        }
    }

    Component {
        id: secondarySwipe

        SwipePage {
            property alias items: secSession.availableItems

            session: SecondarySession {
                id: secSession
            }

            onFinished: pageStack.push(secondaryQrCode, {content: secSession.positives.join(";")})
        }
    }

    Component {
        id: scanPage
        ScanPage {
            onScanned: result => pageStack.push(secondarySwipe, {items: result.split(";")})
        }
    }

    ColumnLayout {
        anchors.centerIn: parent

        Button {
            text: i18n("Start Session")
            onClicked: pageStack.push(Qt.resolvedUrl("CollectionListPage.qml"))
        }

        Button {
            text: i18n("Join Session")
            onClicked: pageStack.push(scanPage)
        }

        Button {
            text: i18n("Manage Collections")
            onClicked: pageStack.push(Qt.resolvedUrl("ManageCollectionsPage.qml"))
        }
    }
}
