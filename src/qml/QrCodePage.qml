/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.15 as Kirigami
import org.kde.prison 1.0 as Prison

import org.kde.pikker 1.0

Kirigami.Page {

    id: root

    title: i18n("Scan this code")

    property alias content: barcode.content

    Prison.Barcode {
        id: barcode

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top

        height: width

        barcodeType: Prison.Barcode.QRCode
    }
}
