/**
 * SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.15 as Kirigami

import org.kde.pikker 1.0

Kirigami.ScrollablePage {
    id: root

    title: i18n("Select Items")

    property var session

    signal finished()

    Kirigami.CardsListView {
        id: clv

        model: root.session.unselectedItems

        delegate: Kirigami.Card {
            contentItem: Label {
                text: modelData
            }
            actions: [
                Kirigami.Action {
                    text: i18n("Yes")
                    iconName: "dialog-ok"
                    onTriggered: {
                        root.session.pickPositive(modelData)
                        if (clv.count == 0) {
                            root.finished()
                        }
                    }
                },
                Kirigami.Action {
                    text: i18n("No")
                    iconName: "dialog-cancel"
                    onTriggered: {
                        root.session.pickNegative(modelData)
                        if (clv.count == 0) {
                            root.finished()
                        }
                    }
                }
            ]
        }
    }
}
