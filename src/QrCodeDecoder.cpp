/**
 * SPDX-FileCopyrightText: 2016-2019 Kaidan developers and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "QrCodeDecoder.h"
// Qt
#include <QColor>
#include <QImage>
// ZXing-cpp
#include <ZXing/ReadBarcode.h>
#include <ZXing/TextUtfEncoding.h>

using namespace ZXing;

QrCodeDecoder::QrCodeDecoder(QObject *parent)
    : QObject(parent)
{
}

void QrCodeDecoder::decodeImage(const QImage &image)
{
    // options for decoding
    DecodeHints decodeHints;

    decodeHints.setFormats(BarcodeFormat::QRCode);
    const auto result = ReadBarcode({image.bits(), image.width(), image.height(), ZXing::ImageFormat::Lum, image.bytesPerLine()}, decodeHints);

    // If a QR code could be found and decoded, emit a signal with the decoded string.
    // Otherwise, emit a signal for failed decoding.
    if (result.isValid())
        emit decodingSucceeded(QString::fromStdString(TextUtfEncoding::ToUtf8(result.text())));
    else
        emit decodingFailed();
}
